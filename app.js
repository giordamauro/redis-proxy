const http = require('http')
const url = require('url')
const winston = require('winston')
const redis = require('redis')
const redisProxy = require('./src/redis-proxy')

// ENV Variables
const PORT = process.env.PORT || 3000
const NODE_ENV = process.env.NODE_ENV || 'development'
const REDIS_PORT = process.env.REDIS_PORT || 6379
const REDIS_HOST = process.env.REDIS_HOST || 'localhost'
const CACHE_TTL = process.env.CACHE_TTL || 300 //  Number of seconds
const LOG_LEVEL = process.env.LOG_LEVEL || 'info'

// Create and config logger
const logger = winston.createLogger({
  level: LOG_LEVEL,
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: 'logs/error.log', level: 'error' }),
    new winston.transports.File({ filename: 'logs/app.log' })
  ]
})
if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: winston.format.simple()
  }))
}

// Create RedisProxy
const redisClient = redis.createClient(REDIS_PORT, REDIS_HOST, { detect_buffers: true })
redisClient.on('error', (err) => {
  logger.error(`Error connecting to redis - ${err.message}`)
})
const proxyClient = new redisProxy.RedisProxy({ redisClient, cacheTTL: CACHE_TTL })
const RedisProxyError = redisProxy.RedisProxyError

//  --------------------------------------------------------------------------------------------------------------------
const ENDPOINT_PATH_URL = '/proxy/pull'
const TEXT_CONTENT_TYPE = { 'Content-Type': 'text/plain' }

const HTTP_NOT_FOUND = 404
const HTTP_INVALID_REQUEST = 400
const HTTP_INTERNAL_ERROR = 500

/**
 * Writes a text/plain message to response, including a status code.

 * @param res Response object
 * @param status Number - HTTP response code
 * @param message - Optional String
 */
const sendMessage = (res, status = HTTP_INVALID_REQUEST, message) => {
  res.writeHead(status, TEXT_CONTENT_TYPE)
  if (message) {
    res.write(message)
  }
  res.end()
}

/**
 * Handles every request issued to this app.
 * Validates the path url for endpoint and presence of 'url' query param.
 * Finally uses RedisProxy to retrieve a possibly cached request.
 * Errors are logged and exposed (except REDIS type error that is assumed to return a normal response from client)

 * @param req HTTP request
 * @param res HTTP response
 */
const requestHandler = (req, res) => {
  // 1. Validate path url
  const reqUrl = url.parse(req.url, true)
  if (reqUrl.pathname !== ENDPOINT_PATH_URL) {
    return sendMessage(res, HTTP_NOT_FOUND, `Unknown path - Try '${ENDPOINT_PATH_URL}'`)
  }

  // 2. Validate 'url' query param
  const proxyUrl = reqUrl.query.url
  if (!proxyUrl) {
    return sendMessage(res, HTTP_INVALID_REQUEST, `Missing 'url' query param`)
  }

  // 3. Send proxy response
  proxyClient.get(proxyUrl, res, (err) => {
    // Handle exceptions
    if (err.type === RedisProxyError.TYPE_CLIENT) {
      sendMessage(res, HTTP_INVALID_REQUEST, err.message)
      return logger.warn(err.message)
    }
    if (err.type === RedisProxyError.TYPE_UNEXPECTED) {
      sendMessage(res, HTTP_INTERNAL_ERROR, err.message)
    }
    logger.error(err.message)
  })
}
//  --------------------------------------------------------------------------------------------------------------------
http.createServer(requestHandler)
  .listen(PORT, (err) => {
    if (err) {
      return logger.error(`Exception starting app on PORT=${PORT} - Error: ${err.message}`)
    }
    logger.info(`Started app - NODE_ENV:'${NODE_ENV}', PORT:${PORT}`)
  })
