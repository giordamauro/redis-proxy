> NodeJS app implementing a proxy server for HTTP GET requests, based on Redis for caching.

## Install for Development

Install with Docker:
```
docker-composer up
npm start
```

### Run as production (with PM2)
```
export NOD_ENV=production && docker-composer up
```


### Configuration Options

* **NOD_ENV**: `production` or defaults to `development`
* **PORT**: default `3000`
* **CACHE_TTL**: `#` number of seconds (default 5 minutes)
* **LOG_LEVEL**: default `info`
* **REDIS_PORT**: default `6379`
* **REDIS_HOST**: default `localhost`


### Scripts
```
npm lint #ESLinter
npm run unit #Jest unit tests

npm test # (runs both)
```

## RedisProxy class

This module exposes the base RedisProxy, created over a redis client instance.

``` javascript
const redis = require('redis')
const redisProxy = require('./src/redis-proxy')

// Create RedisProxy

const redisClient = redis.createClient(REDIS_PORT, REDIS_HOST, { detect_buffers: true })

redisClient.on('error', (err) => {
  logger.error(`Error connecting to redis - ${err.message}`)
})

const proxyClient = new redisProxy.RedisProxy({ redisClient, cacheTTL: CACHE_TTL })
```

## RedisProxyError class

Defines a basic error type for RedisProxy errors. Contains fields for `message`, `type` and `cause`.
There are 3 types of errors defined: `TYPE_CLIENT`, `TYPE_REDIS` and `TYPE_UNEXPECTED`

### Example

``` javascript
const RedisProxyError = redisProxy.RedisProxyError

if (err.type === RedisProxyError.TYPE_UNEXPECTED) {
    logger.error(err.message)
}
```

## Notes
To continue / improve:
 - Fix supertest tests / Mock http, https in unit test
 - Implement a Promise / EventEmitter interface
 - Compare Promises approach implementation with Benchmark tests
