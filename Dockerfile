FROM node:10

# Create app directory
RUN mkdir -p /usr/src/proxy-app
WORKDIR /usr/src/proxy-app

# Install app dependencies
COPY package.json package-lock.json entrypoint.sh /usr/src/proxy-app/

RUN npm install
RUN npm i -g pm2

ENTRYPOINT ["/usr/src/proxy-app/entrypoint.sh"]
