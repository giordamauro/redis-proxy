const redis = require('redis')
const redisProxy = require('../redis-proxy')

jest.mock('redis-rstream', () => jest.fn().mockImplementation(() => ({ pipe: jest.fn() })))

describe('RedisProxy', () => {
  let redisClient
  beforeAll(() => {
    redisClient = redis.createClient(null, null, { detect_buffers: true })
  })

  afterAll(() => {
    redisClient.quit()
  })

  describe('constructor', () => {
    it('should fail if no redisClient', () => {
      let error, proxy
      try {
        proxy = new redisProxy.RedisProxy()
      } catch (e) {
        error = e
      }
      expect(error).toBeDefined()
      expect(proxy).toBeUndefined()
    })
    it('should assign default params', () => {
      const proxy = new redisProxy.RedisProxy({ redisClient })

      expect(proxy.redisClient).toBeDefined()
      expect(proxy.cacheTTL).toBe(300)
    })
  })

  describe('getAndStore', () => {
    afterEach(() => {
      jest.restoreAllMocks()
    })
    it('should call client and store to redis', () => {
      const proxy = new redisProxy.RedisProxy({ redisClient, cacheTTL: 5 })

      const mockRes = {
        writeHead: jest.fn(),
        write: jest.fn(),
        end: jest.fn()
      }
      const mockCallback = jest.fn()

      redisClient.setex = jest.fn()
      redisClient.set = jest.fn()
      proxy.getAndStore('https://www.google.com', mockRes, mockCallback)

      // Wait for client to finish request (https should be mocked)
      setTimeout(() => {
        expect(mockRes.writeHead).toHaveBeenCalledTimes(1)
        expect(mockRes.write).toHaveBeenCalled()
        expect(mockRes.end).toHaveBeenCalledTimes(1)

        expect(redisClient.setex).toHaveBeenCalledTimes(1)
        expect(redisClient.set).toHaveBeenCalledTimes(1)

        expect(mockCallback).not.toHaveBeenCalled()
      }, 1000)
    })
    it('should call callback with error', async () => {
      const proxy = new redisProxy.RedisProxy({ redisClient, cacheTTL: 5 })

      const mockRes = {
        writeHead: jest.fn(),
        write: jest.fn(),
        end: jest.fn()
      }
      redisClient.setex = jest.fn()
      redisClient.set = jest.fn()

      let error
      const mockCallback = (err) => { error = err }

      proxy.getAndStore('http://localhost:3000/inexistent', mockRes, mockCallback)

      // Wait for client to finish request (https should be mocked)
      setTimeout(() => {
        expect(mockRes.writeHead).not.toHaveBeenCalled()
        expect(mockRes.write).not.toHaveBeenCalled()
        expect(mockRes.end).not.toHaveBeenCalled()

        expect(redisClient.setex).not.toHaveBeenCalled()
        expect(redisClient.set).not.toHaveBeenCalled()

        expect(mockCallback).toHaveBeenCalledTimes(1)
        expect(error).toBeDefined()
        expect(error instanceof redisProxy.RedisProxyError).toBeTruthy()
        expect(error.type).toBe('client')
      }, 1000)
    })
  })

  describe('get', () => {
    afterEach(() => {
      jest.restoreAllMocks()
    })
    it('should call getAndStore if key doesn\'t exist', () => {
      const proxy = new redisProxy.RedisProxy({ redisClient, cacheTTL: 5 })

      const mockRes = {
        writeHead: jest.fn(),
        write: jest.fn(),
        end: jest.fn()
      }
      const mockCallback = jest.fn()

      redisClient.setex = jest.fn()
      redisClient.set = jest.fn()
      redisClient.exists = jest.fn().mockImplementation((key, callback) => callback(null, false))

      proxy.get('https://www.google.com', mockRes, mockCallback)

      // Wait for client to finish request (https should be mocked)
      setTimeout(() => {
        expect(mockRes.writeHead).toHaveBeenCalledTimes(1)
        expect(mockRes.write).toHaveBeenCalled()
        expect(mockRes.end).toHaveBeenCalledTimes(1)

        expect(redisClient.setex).toHaveBeenCalledTimes(1)
        expect(redisClient.set).toHaveBeenCalledTimes(1)

        expect(mockCallback).not.toHaveBeenCalled()
      }, 1000)
    })
    it('should fetch from redis if key exists', () => {
      const proxy = new redisProxy.RedisProxy({ redisClient, cacheTTL: 5 })

      const mockRes = {
        writeHead: jest.fn(),
        once: jest.fn(),
        on: jest.fn(),
        emit: jest.fn(),

        write: jest.fn(),
        end: jest.fn()
      }
      const mockCallback = jest.fn()

      redisClient.exists = jest.fn().mockImplementation((key, callback) => callback(null, true))
      redisClient.get = jest.fn().mockImplementation((key, callback) => callback(null, '{}'))
      redisClient.setex = jest.fn()
      redisClient.set = jest.fn()

      proxy.get('https://www.google.com', mockRes, mockCallback)

      // Wait for client to finish request (https should be mocked)
      setTimeout(() => {
        expect(mockRes.writeHead).toHaveBeenCalledTimes(1)
        expect(mockRes.once).toHaveBeenCalled()
        expect(mockRes.emit).toHaveBeenCalled()
        expect(mockRes.write).not.toHaveBeenCalled()
        expect(mockRes.end).not.toHaveBeenCalled()

        expect(redisClient.exists).toHaveBeenCalledTimes(1)
        expect(redisClient.get).toHaveBeenCalledTimes(1)
        expect(redisClient.setex).not.toHaveBeenCalled()
        expect(redisClient.set).not.toHaveBeenCalled()

        expect(mockCallback).not.toHaveBeenCalled()
      }, 1000)
    })
    it('should still fetch from client if redis has errors', () => {
      const proxy = new redisProxy.RedisProxy({ redisClient, cacheTTL: 5 })

      const mockRes = {
        writeHead: jest.fn(),
        once: jest.fn(),
        on: jest.fn(),
        emit: jest.fn(),

        write: jest.fn(),
        end: jest.fn()
      }

      let error
      const mockCallback = (err) => { error = err }

      redisClient.exists = jest.fn().mockImplementation((key, callback) => callback(null, true))
      redisClient.get = jest.fn().mockImplementation((key, callback) => callback(new Error('test Error')))
      redisClient.setex = jest.fn()
      redisClient.set = jest.fn()

      proxy.get('https://www.google.com', mockRes, mockCallback)

      // Wait for client to finish request (https should be mocked)
      setTimeout(() => {
        expect(mockRes.writeHead).toHaveBeenCalledTimes(1)
        expect(mockRes.once).not.toHaveBeenCalled()
        expect(mockRes.emit).not.toHaveBeenCalled()
        expect(mockRes.write).toHaveBeenCalled()
        expect(mockRes.end).toHaveBeenCalled()

        expect(redisClient.exists).toHaveBeenCalledTimes(1)
        expect(redisClient.get).toHaveBeenCalledTimes(1)
        expect(redisClient.setex).toHaveBeenCalledTimes(1)
        expect(redisClient.set).toHaveBeenCalledTimes(1)

        expect(mockCallback).toHaveBeenCalledTimes(1)
        expect(error).toBeDefined()
        expect(error instanceof redisProxy.RedisProxyError).toBeTruthy()
        expect(error.type).toBe('redis')
      }, 1000)
    })
  })
})
