'use strict'

const rstream = require('redis-rstream')
const http = require('http')
const https = require('https')

const KEY_DATA_PREFIX = 'data'
const KEY_HEADERS_PREFIX = 'headers'

const HTTP_OK = 200

/**
 * Exception class used by RedisProxy to return in callbacks with error
 */
class RedisProxyError extends Error {
  /**
   * Constructor - Message and type should always be present
   * Type should be one of RedisProxyError.TYPE_* constants
   */
  constructor ({ message, type, cause }) {
    super(message)
    this.type = type
    this.cause = cause
  }
}
RedisProxyError.TYPE_UNEXPECTED = 'unexpected'
RedisProxyError.TYPE_CLIENT = 'client'
RedisProxyError.TYPE_REDIS = 'redis'

/**
 * This class represents a cache layer for HTTP requests, using redis as storage.
 * Response is sent to a Writable stream as soon as possible, assuming non-errors first.
 * In case of errors with Redis, a client request is issued to keep service available.
 */
class RedisProxy {
  /**
   * Constructor - Creates an instance of redis-node client
   * @param redisClient - An instance of redis client
   * @param cacheTTL Number of seconds - Defaults to 5 minutes Time to live
   */
  constructor ({ redisClient, cacheTTL = 300 }) {
    this.redisClient = redisClient
    this.cacheTTL = cacheTTL
  }

  /**
   * Main method to be used to retrieve a possible Cached request.
   * Request url is first checked in redis for existence, and then written to stream
   * Response headers are also cached and included in the response

   * @param proxyUrl - String url for the service to fetch
   * @param res - Writable stream
   * @param callback - For handling errors
   */
  get (proxyUrl, res, callback) {
    this.redisClient.exists(`${KEY_DATA_PREFIX}:${proxyUrl}`, (err, keyExists) => {
      if (!err && keyExists) {
        //  Cache hit
        return this._sendFromCache(proxyUrl, res, callback)
      }
      // Default send and store in cache
      this.getAndStore(proxyUrl, res, callback)

      // Handle errors
      if (err) {
        callback(new RedisProxyError({
          message: `Exception in redis.EXISTS for key='${KEY_DATA_PREFIX}:${proxyUrl}' - Error: ${err.message}`,
          type: RedisProxyError.TYPE_REDIS,
          cause: err
        }))
      }
    })
  }

  /**
   * Retrieves request content by HTTP client as soon as information is available,
   * and then stores response headers and content in Cache for later use

   * NOTE: request headers? Requests headers are not considered for now, only URL
   * @param proxyUrl - String url for the service to fetch
   * @param res - Writable stream
   * @param callback - For handling errors
   */
  getAndStore (proxyUrl, res, callback) {
    const client = (proxyUrl.startsWith('http:')) ? http : https
    client.get(proxyUrl, (resp) => {
      const data = []
      res.writeHead(resp.statusCode, resp.headers)

      // Write to response and store data chunk in variable
      resp.on('data', (chunk) => {
        res.write(chunk)
        data.push(chunk)
      })
      // Finish response. Save results to redis.
      resp.on('end', () => {
        res.end()

        // Store headers and data with TTL expire time
        this.redisClient.setex(`${KEY_DATA_PREFIX}:${proxyUrl}`, this.cacheTTL, Buffer.concat(data))
        this.redisClient.set(`${KEY_HEADERS_PREFIX}:${proxyUrl}`, JSON.stringify(resp.headers))
      })
      // Unexpected exception
      resp.on('error', (err) => {
        callback(new RedisProxyError({
          message: `Exception sending http response for url='${proxyUrl}' - Error: ${err.message}`,
          type: RedisProxyError.TYPE_UNEXPECTED,
          cause: err
        }))
      })
    }).on('error', (err) => {
      callback(new RedisProxyError({
        message: `Exception in http client for url='${proxyUrl}' - Error: ${err.message}`,
        type: RedisProxyError.TYPE_CLIENT,
        cause: err
      }))
    })
  }

  /**
   * Obtains data stored for proxyUrl KEY, sets response headers and HTTP OK status code (200),
   * and sends the contents as stream.
   * In case of error with redis, a client request is sent instead (and error through callback)

   * @private
   */
  _sendFromCache (proxyUrl, res, callback) {
    this.redisClient.get(`${KEY_HEADERS_PREFIX}:${proxyUrl}`, (err, responseHeaders) => {
      if (!err) {
        //  Set headers and send response data as stream
        res.writeHead(HTTP_OK, JSON.parse(responseHeaders))
        rstream(this.redisClient, `${KEY_DATA_PREFIX}:${proxyUrl}`).pipe(res)
        return
      }
      // Default to send and store in cache
      this.getAndStore(proxyUrl, res, callback)

      // Handle errors
      callback(new RedisProxyError({
        message: `Exception in redis.GET headers for key='${KEY_HEADERS_PREFIX}:${proxyUrl}' - Error: ${err.message}`,
        type: RedisProxyError.TYPE_REDIS,
        cause: err
      }))
    })
  }
}
module.exports = {
  RedisProxy: RedisProxy,
  RedisProxyError: RedisProxyError
}
