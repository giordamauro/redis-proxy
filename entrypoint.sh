#!/bin/bash
if [ "$NODE_ENV" = "production" ]; then
  echo "Running NODE_ENV production"
  pm2 --no-daemon start app.js -i max
else
  echo "Running NODE_ENV development"
  tail -f logs/app.log
fi
